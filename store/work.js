export const actions = {
  async getAllWorks() {
    const res = await this.$axios.$get(`/wp-json/wp/v2/works`)
    return res
  },
  async getWorkSlug({}, {
    workSlug
  }) {
    const res = await this.$axios.$get(`/wp-json/wp/v2/works?slug=${encodeURI(workSlug)}`)
    return res
  },
  async getAllServices() {
    const res = await this.$axios.$get(`/wp-json/wp/v2/categories`)
    return res
  }
}

export const mutations = {}
