import pkg from '~/package'
import urls from '~/services/apiUrl'

export const state = () => ({
  title: `${pkg.description}`,
  description: `${pkg.ogdescription}`
})

export const actions = {
  async sendEmail({
    commit
  }, {
    name,
    email,
    type,
    description,
  }) {
    const res = await this.$axios.$post(urls.sendEmail, {
      name,
      email,
      type,
      description
    })
    return res
  },
}
