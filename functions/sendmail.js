const functions = require('firebase-functions')
const express = require('express')
const app = express()
const cors = require('cors')({
  origin: true
})
const nodemailer = require('nodemailer')
const prvk = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDhci5K+pWKli3/\nKXSgKhR1/+cIUmELSSdyqrU5TzlI1EHXqGLqNSvu1wUThPoHn2MTB+gFYArt7wzS\n1QT31ZCfU+wwsJ1htphNTOjOBgzyeMjQuAofdTOcUBo3BUuXnBR7mVGYu2nZA7uP\nH5oeEDxW1nUDOea/v7GJBjpk9pWY2EjPapDmzgHA4m1TwEEMDQkoVId1OLpcSD/E\nPJ/blffL/rdpPTIgEZNb+Xfek7Go22N+b/cFpEZiVlV8CIlr4zaPPphHlGuVl1yS\np/H+PuCu0p5iJK1hsWlIaeY57TJI4jGWYIRRTfuyRBVLm19AIKcP/Fns4jyn5W2A\n1fIaIhwjAgMBAAECggEALpya8amprYOrcuwxTsQkgtkr/U56/ghb/ZtJ6NV2dJZe\nHqaT/PSNZs5SIF0IDPdpVo/16taWq+cW8ahVZKBlTGvfk5mQkJleF+1k3OPnXWKX\nsn+6qBJlX1Y4dVPE802z9nIWqstk8ReSed8A0GJu901T2Bzqlo6duETvYdyRxY3e\n730mE9Mn4teeZ2gdzZ3H3PnbXs2kWiBH6msoPBR4D0TX0hUHQGPVG3SxNJDbuVvD\nXdPX8Xc0Yv6gUMyfn/xAXDo4bD6h8mtE+sUnNpz6pKLHKCwxnNAnLjGZde4kqUBm\nsmWj0NSThuswAXBHCQp7ymLzu2e/Gxs132aXlwqF0QKBgQD2uwMVnzThvVFtdUyB\nZJ60EXkhqm0Z7Zi7RsYZKOAvoyeA7tpYNd8bc7Sms1gKJF9GU+X3lVYWZnP367kT\nGL86QLEmW40XHZ8kz+YUcVd0DkzhubRHJ8BzGHeqzdpStjRvy6KrFoccbgsm3dY8\n0zwscdRRa14yDnDskRth1x+oJwKBgQDp6nXPhiX/I+H9NF4cudcyw+wOALwTJ6x+\nbl256ZmNmKHgTv3mUdLXcZY7vUZN9e2ST3iLhjzFWR7zDk8Timx9hp2uQeVurBdd\nZRVVwIQZWqEZEIOPlTyST/lXqDyKQ38+UchGAazBWXt+J6JJbvEoy3Ke2X3MDCrX\nHV2NswFNpQKBgHGzApvfLyZr8O9JI8VpEf2XI8QQ3pAZPs3msnl7rbwmKpku+O23\nfaYP7wr3ELYzxrgJB6vNZMuT0RNJzJLxT4ZPs5Mik2U4oFL9f2CiTpPL10AaLOqx\nde1UNClBDcAZ+RYfuEmxavf9+iOiDQ0gptUwa5BNsy3MG9vYjs7KrOr3AoGABUHo\nzfZE6lwjGy7PIrQmmxR+PL5Ve9S7csgGQUR32A/jK4h6+m9CXudFqigR6XSql0xD\n+FbXDNR49puOuXC2CYg6CQ2gG4GwsL+x4BCIp40667ZMTrDbAKrdftPgMHdd58es\nya4F9P6O4QIBCsd8kh8of1uCEVEAsX8OeD3avKUCgYAcnMwGx8ErOKwG9VbhQgeZ\nfIt3MpUM6RW4yXkPwMOFwwwiRMnSqu1YsHy3BJtq649540RZ4kBRJvjEN71GrCE3\nHi2QmTKvgJt2K78472wsAdeBMQEdqZj8xe/m9sAmb+j1d5zyklnok6xDxc9Vy3Yo\n+vHRyd3nyfQhFiStdOGlaQ==\n-----END PRIVATE KEY-----\n"

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    type: 'OAuth2',
    user: 'hello@5lab.co',
    serviceClient: "114303371213377880467",
    privateKey: prvk,
  }
})

const mailOptions = { // sender address
  to: 'hello@5lab.co', // list of receivers
  subject: 'Contact Form - 5Lab' // Subject line
}

app.use(cors)
app.get('/', (req, res) => {
  return res.send('test 1')
})
app.post('/', (req, res) => {
  if (req.method === 'POST' && req.body.html) {
    mailOptions.sender = req.body.email
    mailOptions.from = req.body.email
    mailOptions.html = req.body.html
    // mailOptions.cc = 'Patcharawan.Pi@bangkokhospital.com'
    mailOptions.replyTo = req.body.email
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        return res.send(err)
      } else {
        return res.send(200)
      }
    })
  }
})

exports = module.exports = functions.https.onRequest(app)
