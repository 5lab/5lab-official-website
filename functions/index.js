if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'ssrapp') {
  exports.ssrapp = require('./ssrapp')
}

if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'sendmail') {
  exports.sendmail = require('./sendmail')
}
