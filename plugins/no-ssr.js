import Vue from 'vue'
import VueFloatLabel from 'vue-float-label'
// import VueThaiAddressInput from '~/modules/vue-thai-address-input'
import VueAgile from 'vue-agile'
import vSelect from 'vue-select'
import BackToTop from 'vue-backtotop'

Vue.use(VueFloatLabel)
// Vue.use(VueThaiAddressInput)
Vue.use(VueAgile)

import VueScrollReveal from 'vue-scroll-reveal'

Vue.use(BackToTop)
// You can also pass in default options
Vue.use(VueScrollReveal, {
  class: 'v-scroll-reveal',
  scale: 1,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
  opacity: 0,
  viewFactor:0.1,
  origin: 'left',
  distance:'50px'
})

Vue.component('v-select', vSelect)
